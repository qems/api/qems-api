plugins {
    id("java-library")
    id("com.netflix.nebula.release") version("18.0.8")
    id("com.netflix.nebula.maven-publish") version("21.0.0")
}

group = "de.offis.e.esc.enaq.qems"

repositories {
    mavenLocal()
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/16916947/packages/maven")
    maven("https://gitlab.com/api/v4/projects/40683768/packages/maven")
}

publishing {
    repositories {
        // To publish the package into our GitLab package repository
        maven {
            url = uri("https://gitlab.com/api/v4/projects/52950396/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

dependencies {

    implementation("org.modelmapper:modelmapper:2.3.8")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    compileOnly("org.projectlombok:lombok:1.18.30")
    annotationProcessor("org.projectlombok:lombok:1.18.30")

    implementation("de.offis.mosaik.api.utils:mosaik-java-utils:0.4.0-dev.0+a146d82")
    testCompileOnly("org.projectlombok:lombok:1.18.30")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.30")
}

tasks.test {
    useJUnitPlatform()
}
