# QEMS-API

This repository is a reference implementation of our general energy entity framework.
It contains the entity classes and helper functions.

The remaining architecture of the readme is the following:
- [General description of the API](#general-api-description)
- [Java implementation specifics](#java-implementation-specifics)
- [How to use the qems-api](#how-to-use-the-qems-api)

## General API description

The framework consists of specific energy entities that can be described with their control and monitoring capabilities.
For a generalization, the entities (in the java implementation) have interfaces, that describe how which inputs, outputs and storage capabilities an entity has.
Input is hereby either demand or consumption and output is the production of the energy type.
The energy entities don't need to implement the interfaces but it makes it easier to describe the energy capabilities.
The following interfaces are implemented:

- InputElectrical: Describes electrical input (consumption/demand)
- OutputElectrical: Describes electrical output (production)
- StorageElectrical: Describes electrical storage specifics
- InputThermal: Describes thermal input. For demand, this is split into two different interfaces
  - InputThermalHeating: Describes thermal heating demand
  - InputThermalDHW: Describes thermal domestic hot water demand
- OutputThermal: Describes thermal output
- StorageThermal: Describes thermal storage capabilities
- AirSource: An input for air based heat pumps, describes the air temperature.
- Transformer: Describes that one energy source can be converted into another energy type. For example, for heat pumps, that transform electrical into thermal energy.

Currently, the following devices can be described:

- Battery: Electrical energy storage system. Has InputElectrical, OutputElectrical, StorageElectrical interfaces.
- Chp: Combined heat and power plant. Has OutputElectrical, OutputThermal interaces. Gas input is currently not modelled.
- Heat pump: Air-water-heat-pump. Has InputElectrical, Transformer and OutputThermal interfaces.
- Heat storage: Thermal energy storage system. Implements InputThermal, OutputThermal and StorageThermal interfaces.
- Household: Entity that represents households. Has InputThermalDHW, InputThermalHeating, InputElectrical interfaces.
- Photovoltaik system: Photovoltaik system representation. Has OutputElectrical interface implemented. Also, more information about the used solar panels and inverter can be added.
- Weather: Describes the weather at a specific location. Fields are for example air temperature, soil temperature, pressure and relative humidity.

Additionally, all fields that represent sensors (and potential actors) are, instead of a single value, a set of control and monitoring capabilities.
The following capabilities can currently be used:
- Controls: Describe single value control capabilities
    - Static: Set a fixed value
    - Curtail: Set a curtailment, where everything under the value is allowed
    - Shutdown: Can be used to shut down or power up a value (not the device)
    - Free: Set a fixed value (currently doubled with static, will be reworked later)
    - Step: Set an integer value that is connected to a step size, i. e.: Set to step 3 with step size of 35 (kw) will set it to 105 (kW)
- ControlSchedule: Can be used to set a schedule for the future. References the corresponding Control capability and contains a dictionary with millisecond key and double value as the control values
- Read: Classes that represent readable capabilities
    - Now: Contains the current sensor value
    - Historic: Contains historic values from the sensor
    - Prediction: Usually contains prediction about the future values of the sensor

The control and monitoring capabilities can be added to capability set fields of the entities/interfaces to better describe what can be done with an entity.
To build your own entity class, see [How to use the qems-api](#how-to-use-the-qems-api).

## Java implementation specifics

There are some implementation details and utility function for the java classes.
These are described here.

### Used language features and external libraries
First of all, control and monitoring classes are encapsuled in the `Read` and `Control` sealed interfaces.
These interfaces allow Java to use some syntactic sugar, for example, pattern matching can be used:
```Java
if (control instanceof Control.Free free) {
    free.setValue(value);
} else if (control instanceof Control.Curtail cur) {
    cur.setValue(value);
} else if (control instanceof Control.Step step) {
    ...
}
```
In later Java versions, this should also be possible for switch-cases.

Another utility we use is Lombok. It enables us to lower the amount of boilerplate code and make the code more concise.
It is used for getter/setter and instance builder.

For mosaik, we also use the [mosaik java generics api](https://gitlab.com/mosaik/api/mosaik-api-java-generics).
All QEMS entities are annotated with the model annotation, to ease the development of mosaik simulators in Java.
The Java simulators also use this api, which helps with the meta-model generation for the mosaik entities.
Also, the simulators do not need to create the mosaik entities itself. 
The entity creation parameter passed from mosaik to the java simulator will automatically create the entity instance and also encapsules messages send to and from mosaik in the step method.
For more information, visit the [mosaik java generics api](https://gitlab.com/mosaik/api/mosaik-api-java-generics).

### Additional utility functions

Since we use mosaik, parameter for the instance generation can only be primitive datatypes or dictionaries.
With these limitations, we had to develop a workaround to be able to describe the control and monitoring capabilities for a device.
For this, we developed the `de.offis.e.esc.qems.api.entities.controls.ParseUtils`, especially the `parseControls` function.
This function transforms a Map or single value into a capability.
As an example, see the following yaml example of a battery of our yaml-configuration for the [ENaQ scenario](https://gitlab.com/qems/scenarios/enaq):
```yaml
  batteryAProxy:
    sim: schedulerSim.Battery
    ...
    inputElectricalPowerW:
        Now: 0.0
        Free:
        Schedule: Free
    ...
```
This will create a Battery instance, where the `inputElectricalPowerW` has three different initialization types:
- Now: Will be initialized with a value of `0.0`
- Free: This control type will be initialized without a value set at start
- Schedule: This will create a `ControlSchedule` that references the `Free` control as their main control capability

The utility function is mainly used when creating entities via mosaik.
When creating entities under other circumstances, this workaround is not needed.
Simply initialize the entity and add the control and monitoring capabilities in a second step.

## How to use the qems-api

If you want to implement your own simulator that uses existing entity definitions, just add the library as dependency:

```gradle
dependency {
    implementation 'de.offis.e.esc.enaq.qems:qems-api:<CURR_VERSION>'
}

maven {
    url 'https://gitlab.com/api/v4/projects/52950396/packages/maven'
}
```

If you want to implement your own energy entity, you can fork this repository.
We would love to get a merge request for new entity types or other features :)

Note however, that new entites cannot be used in conjuction with the [net-scheduler](https://gitlab.com/qems/services/connectors/net-scheduler), as all devices currently need their own implementation.
We are working on easing the implementation effort needed for new entity types.

If you have further questions, please open an issue in this repository.
