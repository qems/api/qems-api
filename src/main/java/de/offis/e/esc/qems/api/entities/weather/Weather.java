package de.offis.e.esc.qems.api.entities.weather;

import de.offis.e.esc.qems.api.entities.BaseEntity;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.mosaik.api.utils.generics.Model;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Model
@Data
@EqualsAndHashCode(callSuper = true)
public class Weather extends BaseEntity {
    String dataProvider;
    //TODO: lat lon?

    SortedSet<Capability> airTemperatureK;
    SortedSet<Capability> soilTemperatureK;
    SortedSet<Capability> pressurePa;
    SortedSet<Capability> relativeHumidity;
    SortedSet<Capability> cloudCover;
    SortedSet<Capability> windSpeedMpS;
    SortedSet<Capability> windDirectionD;
    SortedSet<Capability> windGustMpS;
    SortedSet<Capability> globalHorizontalIrradianceWhoM2;

    @Builder
    @Model.Constructor
    public Weather(@Model.Id @Model.Param("id") String id,
                   @Model.Param("dataProvider") String dataProvider,
                   @Model.Param("airTemperatureK") Map<String, Object> airTemperatureK,
                   @Model.Param("soilTemperatureK") Map<String, Object> soilTemperatureK,
                   @Model.Param("pressurePa") Map<String, Object> pressurePa,
                   @Model.Param("relativeHumidity") Map<String, Object> relativeHumidity,
                   @Model.Param("cloudCover") Map<String, Object> cloudCover,
                   @Model.Param("windSpeedMpS") Map<String, Object> windSpeedMpS,
                   @Model.Param("windDirectionD") Map<String, Object> windDirectionD,
                   @Model.Param("windGustMpS") Map<String, Object> windGustMpS,
                   @Model.Param("globalHorizontalIrradianceWhoM2") Map<String, Object> globalHorizontalIrradianceWhoM2) {
        setId(id);
        setDataProvider(dataProvider);

        Capability.setCapabilities(airTemperatureK, this::setAirTemperatureK);
        Capability.setCapabilities(soilTemperatureK, this::setSoilTemperatureK);
        Capability.setCapabilities(pressurePa, this::setPressurePa);
        Capability.setCapabilities(relativeHumidity, this::setRelativeHumidity);
        Capability.setCapabilities(cloudCover, this::setCloudCover);
        Capability.setCapabilities(windSpeedMpS, this::setWindSpeedMpS);
        Capability.setCapabilities(windDirectionD, this::setWindDirectionD);
        Capability.setCapabilities(windGustMpS, this::setWindGustMpS);
        Capability.setCapabilities(globalHorizontalIrradianceWhoM2, this::setGlobalHorizontalIrradianceWhoM2);
    }
}
