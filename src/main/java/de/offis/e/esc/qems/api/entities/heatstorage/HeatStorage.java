package de.offis.e.esc.qems.api.entities.heatstorage;

import de.offis.e.esc.qems.api.entities.BaseEntity;
import de.offis.e.esc.qems.api.entities.Location;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.interfaces.InputThermal;
import de.offis.e.esc.qems.api.entities.interfaces.OutputThermal;
import de.offis.e.esc.qems.api.entities.interfaces.StorageThermal;
import de.offis.mosaik.api.utils.generics.Model;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

@Data
@Model
@EqualsAndHashCode(callSuper = true)
public class HeatStorage extends BaseEntity implements StorageThermal, InputThermal, OutputThermal {
    Double inputThermalMin;
    Double inputThermalMax;
    SortedSet<Capability> inputThermalPowerW;

    Double outputThermalMin;
    Double outputThermalMax;
    SortedSet<Capability> outputThermalPowerW;

    Double storageThermalCapacityWh;
    SortedSet<Capability> storageThermalSoC;
    SortedSet<Capability> storageThermalSoCWh;
    Double storageThermalSelfDischarge;
    Double tempTarget;
    Double tempReference;
    SortedSet<Capability> temp;
    @Builder
    @Model.Constructor
    public HeatStorage(@Model.Id @Model.Param("id") String id,
                       @Model.Param("outputThermalMax") Double outputThermalMax,
                       @Model.Param("capacityWh") Double capacityWh,
                       @Model.Param("selfDischarge") Double selfDischarge,
                       @Model.Param("tempTarget") Double tempTarget,
                       @Model.Param("tempReference") Double tempReference,
                       @Model.Param("latitude") Double latitude,
                       @Model.Param("longitude") Double longitude,
                       @Model.Param("altitude") Double altitude,
                       @Model.Param("outputThermalPowerW") Map<String, Object> outputThermalPowerW,
                       @Model.Param("inputThermalPowerW") Map<String, Object> inputThermalPowerW,
                       @Model.Param("storageThermalSoC") Map<String, Object> storageThermalSoC,
                       @Model.Param("storageThermalSoCWh") Map<String, Object> storageThermalSoCWh,
                       @Model.Param("temp") Map<String, Object> temp
    ) {
        setId(id);
        setInputThermalMin(0d);
        setInputThermalMax(outputThermalMax);
        Capability.setCapabilities(inputThermalPowerW, this::setInputThermalPowerW);

        Capability.setCapabilities(storageThermalSoCWh, this::setStorageThermalSoCWh);
        Capability.setCapabilities(storageThermalSoC, this::setStorageThermalSoC);
        setStorageThermalCapacityWh(capacityWh);
        setStorageThermalSelfDischarge(selfDischarge);

        setOutputThermalMin(0d);
        setOutputThermalMax(outputThermalMax);
        Capability.setCapabilities(outputThermalPowerW, this::setOutputThermalPowerW);

        setTempTarget(tempTarget);
        setTempReference(tempReference);

        Capability.setCapabilities(outputThermalPowerW, this::setTemp);

        var location = Location.builder()
                .altitude(altitude)
                .latitude(latitude)
                .longitude(longitude)
                .build();
        setLocation(location);
    }
}
