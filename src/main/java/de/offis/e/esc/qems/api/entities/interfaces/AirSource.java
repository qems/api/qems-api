package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface AirSource {
    SortedSet<Capability> getSourceTemperature();
    void setSourceTemperature(SortedSet<Capability> value);
}
