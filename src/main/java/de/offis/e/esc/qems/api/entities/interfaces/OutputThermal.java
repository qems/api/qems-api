package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface OutputThermal {

    Double getOutputThermalMin();
    void setOutputThermalMin(Double val);
    Double getOutputThermalMax();
    void setOutputThermalMax(Double val);
    SortedSet<Capability> getOutputThermalPowerW();
    void setOutputThermalPowerW(SortedSet<Capability> value);
}
