package de.offis.e.esc.qems.api.entities.controls;

import org.modelmapper.ModelMapper;

import java.util.Map;
import java.util.SortedSet;
import java.util.function.Consumer;

public class ParseUtils {
    private static ModelMapper modelMapper = new ModelMapper();

    private ParseUtils() {
    }

    public static Capability parseControls(String className, Object initVal, SortedSet<Capability> capabilities) {
        switch (className) {
            case "Free" -> {
                Control.Free f = new Control.Free();
                fillControl(initVal, f, f::setValue);
                return f;
            }
            case "Static" -> {
                Control.Static f = new Control.Static();
                fillControl(initVal, f, f::setValue);
                return f;
            }
            case "Curtail" -> {
                Control.Curtail f = new Control.Curtail();
                fillControl(initVal, f, f::setValue);
                return f;
            }
            case "Shutdown" -> {
                Control.Shutdown f = new Control.Shutdown();
                fillControl(initVal, f, f::setShouldShutdown);
                return f;
            }
            case "Step" -> {
                Control.Step f = new Control.Step();
                fillControl(initVal, f, f::setValue);
                return f;
            }
            case "Now" -> {
                Read.Now f = new Read.Now();
                fillControl(initVal, f, f::setValue);
                return f;
            }
            case "Historic" -> {
                Read.Historic f = new Read.Historic();
                fillControl(initVal, f, f::setHistory);
                return f;
            }
            case "Prediction" -> {
                Read.Prediction f = new Read.Prediction();
                fillControl(initVal, f, f::setForecast);
                return f;
            }
            case "Schedule" -> {
                ControlSchedule f = ControlSchedule.builder().build();
                Map<String, Object> control = null;
                if (initVal != null && initVal instanceof Map) {
                    control = ((Map<String, Object>)initVal);
                    control.forEach((s, object) -> fillControl(getControl(s, capabilities), f, f::setControl)); // Should only be one but else it will be the last

                } else if (initVal != null) {
                    Object controlCapability = getControl((String)initVal, capabilities);
                    fillControl(controlCapability, f, f::setControl);
                }
                return f;
            }
            default -> throw new IllegalArgumentException("Cannot parse " + className + " into one of the existing control or read capabilities");
        }
    }

    private static Object getControl(String typeName, SortedSet<Capability> capabilities) {
        return capabilities.stream().filter(cap -> cap.getCapability().equals(typeName)).findFirst().orElseThrow();
    }

    private static <T extends Capability, R> void fillControl(Object initVal, T f, Consumer<R> setMethod) {
        if(initVal instanceof Map) {
            modelMapper.map(initVal, f);
        } else {
            //noinspection unchecked
            setMethod.accept((R)initVal);
        }
    }
}
