package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface StorageElectrical {

    Double getStorageElectricalCapacityWh();
    void setStorageElectricalCapacityWh(Double value);
    SortedSet<Capability> getStorageElectricalSoC();
    void setStorageElectricalSoC(SortedSet<Capability> value);
    SortedSet<Capability> getStorageElectricalSoCWh();
    void setStorageElectricalSoCWh(SortedSet<Capability> value);

    Double getStorageElectricalSelfDischarge();
    void setStorageElectricalSelfDischarge(Double value);
}
