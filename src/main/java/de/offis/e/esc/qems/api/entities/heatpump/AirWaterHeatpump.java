package de.offis.e.esc.qems.api.entities.heatpump;

import de.offis.e.esc.qems.api.entities.BaseEntity;
import de.offis.e.esc.qems.api.entities.Location;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.interfaces.AirSource;
import de.offis.e.esc.qems.api.entities.interfaces.InputElectrical;
import de.offis.e.esc.qems.api.entities.interfaces.OutputThermal;
import de.offis.e.esc.qems.api.entities.interfaces.Transformer;
import de.offis.e.esc.qems.api.entities.photovoltaic.PVInformation;
import de.offis.mosaik.api.utils.generics.Model;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

@Data

@Model
@EqualsAndHashCode(callSuper = true)
public class AirWaterHeatpump extends BaseEntity implements AirSource, InputElectrical, OutputThermal, Transformer {
    SortedSet<Capability> sourceTemperature;

    Double inputElectricalMin;
    Double inputElectricalMax;
    SortedSet<Capability> inputElectricalPowerW;

    Double outputThermalMin;
    Double outputThermalMax;
    SortedSet<Capability> outputThermalPowerW;

    Double copDefinition;
    SortedSet<Capability> cop;

    SortedSet<Capability> tempIn;
    SortedSet<Capability> tempOut;

    @Builder
    @Model.Constructor
    public AirWaterHeatpump(@Model.Id @Model.Param("id") String id,
                            @Model.Param("inputElectricalMax") Double inputElectricalMax,
                            @Model.Param("outputThermalMax") Double outputThermalMax,
                            @Model.Param("A5W35Cop") Double a5w35Cop,
                            @Model.Param("outputThermalPowerW") Map<String, Object> outputThermalPowerW,
                            @Model.Param("inputElectricalPowerW") Map<String, Object> inputElectricalPowerW,
                            @Model.Param("sourceTemperature") Map<String, Object> sourceTemperature,
                            @Model.Param("cop") Map<String, Object> cop,
                            @Model.Param("tempIn") Map<String, Object> tempIn,
                            @Model.Param("tempOut") Map<String, Object> tempOut,
                            @Model.Param("latitude") Double latitude,
                            @Model.Param("longitude") Double longitude,
                            @Model.Param("altitude") Double altitude) {
        setId(id);
        setInputElectricalMin(0d);
        setInputElectricalMax(inputElectricalMax);
        Capability.setCapabilities(inputElectricalPowerW, this::setInputElectricalPowerW);

        setOutputThermalMin(0.0d);
        setOutputThermalMax(outputThermalMax);
        Capability.setCapabilities(outputThermalPowerW, this::setOutputThermalPowerW);

        Capability.setCapabilities(cop, this::setCop);
        setCopDefinition(a5w35Cop);

        Capability.setCapabilities(sourceTemperature, this::setSourceTemperature);
        Capability.setCapabilities(tempIn, this::setTempIn);
        Capability.setCapabilities(tempOut, this::setTempOut);

        var location = Location.builder()
                .altitude(altitude)
                .latitude(latitude)
                .longitude(longitude)
                .build();
        setLocation(location);

    }
}
