package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface StorageThermal {
    Double getStorageThermalCapacityWh();
    void setStorageThermalCapacityWh(Double value);
    SortedSet<Capability> getStorageThermalSoC();
    void setStorageThermalSoC(SortedSet<Capability> value);
    SortedSet<Capability> getStorageThermalSoCWh();
    void setStorageThermalSoCWh(SortedSet<Capability> value);

    Double getStorageThermalSelfDischarge();
    void setStorageThermalSelfDischarge(Double value);

    Double getTempTarget();
    Double getTempReference();

    void setTempTarget(Double value);
    void setTempReference(Double value);
}
