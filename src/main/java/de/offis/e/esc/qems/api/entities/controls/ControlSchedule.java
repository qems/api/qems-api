package de.offis.e.esc.qems.api.entities.controls;

import de.offis.mosaik.api.utils.generics.Model;
import lombok.Builder;
import lombok.Data;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

@Model
@Data
@Builder
public final class ControlSchedule implements Capability {
    Map<Long, Double> schedule;
    Control control;
    String usedAlgorithm;
    @Builder.Default String id = UUID.randomUUID().toString();

    @Builder
    @Model.Constructor
    public ControlSchedule(@Model.Param("schedule") Map<Long, Double> schedule,
                           @Model.Param("control") Control control,
                           @Model.Param("usedAlgorithm") String usedAlgorithm,
                           @Model.Param("id") String id) {
        this.schedule = schedule;
        this.control = control;
        this.usedAlgorithm = usedAlgorithm;
        this.id = id;
    }

    public ControlSchedule() {}
}
