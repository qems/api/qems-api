package de.offis.e.esc.qems.api.entities.photovoltaic;

import de.offis.e.esc.qems.api.entities.BaseEntity;
import de.offis.e.esc.qems.api.entities.Location;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.mosaik.api.utils.generics.Model;
import de.offis.e.esc.qems.api.entities.interfaces.OutputElectrical;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Model

@Data
@EqualsAndHashCode(callSuper = true)
public class PhotovoltaicSystem extends BaseEntity implements OutputElectrical {
    Double outputElectricalMin;
    Double outputElectricalMax;
    SortedSet<Capability> outputElectricalPowerW;

    PVInformation information;

    @Builder
    @Model.Constructor
    public PhotovoltaicSystem(@Model.Id @Model.Param("id") String id,
                              @Model.Param("outputElectricalPowerW") Map<String, Object> outputElectricalPowerW,
                              @Model.Param("maxElectricalPowerOutputW") Double maxElectricalPowerOutputW,
                              @Model.Param("stringsPerInverter") Long stringsPerInverter,
                              @Model.Param("modulesPerString") Long modulesPerString,
                              @Model.Param("moduleName") String moduleName,
                              @Model.Param("inverterName") String inverterName,
                              @Model.Param("latitude") Double latitude,
                              @Model.Param("longitude") Double longitude,
                              @Model.Param("altitude") Double altitude) {
        setId(id);
        setOutputElectricalMin(0d);
        setOutputElectricalMax(maxElectricalPowerOutputW);
        Capability.setCapabilities(outputElectricalPowerW, this::setOutputElectricalPowerW);

        var location = Location.builder()
                .altitude(altitude)
                .latitude(latitude)
                .longitude(longitude)
                .build();
        setLocation(location);
        var information = PVInformation.builder()
                .inverterName(inverterName)
                .moduleName(moduleName)
                .stringsPerInverter(stringsPerInverter)
                .modulesPerString(modulesPerString)
                .build();
        setInformation(information);
    }
}
