package de.offis.e.esc.qems.api.entities.household;

import de.offis.e.esc.qems.api.entities.BaseEntity;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.interfaces.InputThermalDhw;
import de.offis.e.esc.qems.api.entities.interfaces.InputThermalHeating;
import de.offis.e.esc.qems.api.entities.interfaces.InputElectrical;
import de.offis.mosaik.api.utils.generics.Model;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;
import java.util.Set;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

@Model
@Data
@EqualsAndHashCode(callSuper = true)
public class Household extends BaseEntity implements InputElectrical, InputThermalHeating, InputThermalDhw {
    Double inputElectricalMin;
    Double inputElectricalMax;
    SortedSet<Capability> inputElectricalPowerW;

    Double inputThermalMax;
    Double inputThermalMin;

    SortedSet<Capability> inputThermalHeatingW;
    SortedSet<Capability> inputThermalDhwW;

    @Model.Suppress
    @Override
    public SortedSet<Capability> getInputThermalPowerW() {
        var set = new TreeSet<>(inputThermalHeatingW);
        set.addAll(inputThermalDhwW);
        return Collections.unmodifiableSortedSet(set);
    }

    @Model.Suppress
    @Override
    public void setInputThermalPowerW(SortedSet<Capability> value) {
        throw new RuntimeException("Cannot set input thermal for households");
    }


    @Builder
    @Model.Constructor
    public Household(@Model.Id @Model.Param("id") String id,
                     @Model.Param("inputElectricalMin") Double inputElectricalMin,
                     @Model.Param("inputElectricalMax") Double inputElectricalMax,
                     @Model.Param("inputElectricalPowerW") Map<String, Object> inputElectricalPowerW,
                     @Model.Param("inputThermalMin") Double inputThermalMin,
                     @Model.Param("inputThermalMax") Double inputThermalMax,
                     @Model.Param("inputThermalHeatingW") Map<String, Object> inputThermalHeatingW,
                     @Model.Param("inputThermalDhwW") Map<String, Object> inputThermalDhwW) {
        setId(id);
        setInputElectricalMin(inputElectricalMin);
        setInputElectricalMax(inputElectricalMax);
        Capability.setCapabilities(inputElectricalPowerW, this::setInputElectricalPowerW);

        setInputThermalMin(inputThermalMin);
        setInputThermalMax(inputThermalMax);
        Capability.setCapabilities(inputThermalDhwW, this::setInputThermalDhwW);
        Capability.setCapabilities(inputThermalHeatingW, this::setInputThermalHeatingW);
    }
}
