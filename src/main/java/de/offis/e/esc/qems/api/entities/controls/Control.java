package de.offis.e.esc.qems.api.entities.controls;

import de.offis.mosaik.api.utils.generics.Model;
import lombok.Data;

import java.util.UUID;

public sealed interface Control extends Capability {
    @Model @Data final class Static implements Control { Double value; String Id = UUID.randomUUID().toString();}
    @Model @Data final class Curtail implements Control {Double value; String Id = UUID.randomUUID().toString();}
    @Model @Data final class Shutdown implements Control {Boolean shouldShutdown; String Id = UUID.randomUUID().toString();}
    @Model @Data final class Free implements Control {Double value; String Id = UUID.randomUUID().toString();}
    @Model @Data final class Step implements Control {Long value; Double stepSize; String Id = UUID.randomUUID().toString();}
}


