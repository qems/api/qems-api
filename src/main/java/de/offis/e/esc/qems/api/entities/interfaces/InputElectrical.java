package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface InputElectrical {
    Double getInputElectricalMin();
    void setInputElectricalMin(Double val);
    Double getInputElectricalMax();
    void setInputElectricalMax(Double val);
    SortedSet<Capability> getInputElectricalPowerW();
    void setInputElectricalPowerW(SortedSet<Capability> value);
}
