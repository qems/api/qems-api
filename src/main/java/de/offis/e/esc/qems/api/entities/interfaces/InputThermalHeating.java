package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.SortedSet;

public interface InputThermalHeating extends InputThermal {
    SortedSet<Capability> getInputThermalHeatingW();
    void setInputThermalHeatingW(SortedSet<Capability> value);
}
