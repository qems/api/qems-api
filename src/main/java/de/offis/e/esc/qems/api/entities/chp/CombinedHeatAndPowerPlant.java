package de.offis.e.esc.qems.api.entities.chp;

import de.offis.e.esc.qems.api.entities.BaseEntity;
import de.offis.e.esc.qems.api.entities.Location;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.mosaik.api.utils.generics.Model;
import de.offis.e.esc.qems.api.entities.interfaces.OutputElectrical;
import de.offis.e.esc.qems.api.entities.interfaces.OutputThermal;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

@Model
@Data
@EqualsAndHashCode(callSuper = true)
public class CombinedHeatAndPowerPlant extends BaseEntity implements OutputElectrical, OutputThermal {
    Double outputThermalMin;
    Double outputThermalMax;
    SortedSet<Capability> outputThermalPowerW;

    Double outputElectricalMin;
    Double outputElectricalMax;
    SortedSet<Capability> outputElectricalPowerW;

    Double electricalCOP;

    @Builder
    @Model.Constructor
    public CombinedHeatAndPowerPlant(@Model.Id @Model.Param("id") String id,
                                     @Model.Param("maxElectricalPowerOutputW") Double maxElectricalPowerOutputW,
                                     @Model.Param("maxThermalPowerOutputW") Double maxThermalPowerOutputW,
                                     @Model.Param("electricalCOP") Double electricalCOP,
                                     @Model.Param("latitude") Double latitude,
                                     @Model.Param("longitude") Double longitude,
                                     @Model.Param("altitude") Double altitude,
                                     @Model.Param("outputElectricalPowerW") Map<String, Object> outputElectricalPowerW,
                                     @Model.Param("outputThermalPowerW") Map<String, Object> outputThermalPowerW
    ) {
        setId(id);
        setOutputThermalMin(0d);
        setOutputThermalMax(maxThermalPowerOutputW);
        Capability.setCapabilities(outputThermalPowerW, this::setOutputThermalPowerW);
        setOutputElectricalMin(0d);
        setOutputElectricalMax(maxElectricalPowerOutputW);
        Capability.setCapabilities(outputElectricalPowerW, this::setOutputElectricalPowerW);
        setElectricalCOP(electricalCOP);

        var location = Location.builder()
                .altitude(altitude)
                .latitude(latitude)
                .longitude(longitude)
                .build();
        setLocation(location);
    }

}
