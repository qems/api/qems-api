package de.offis.e.esc.qems.api.entities.photovoltaic;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PVInformation {
    Long stringsPerInverter;
    Long modulesPerString;
    String moduleName;
    String inverterName;
}
