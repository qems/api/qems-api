package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.SortedSet;

public interface InputThermalDhw extends InputThermal {
    SortedSet<Capability> getInputThermalDhwW();
    void setInputThermalDhwW(SortedSet<Capability> value);
}
