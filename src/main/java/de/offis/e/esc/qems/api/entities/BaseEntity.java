package de.offis.e.esc.qems.api.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public abstract class BaseEntity implements Comparable<BaseEntity> {
    String group;
    String id;
    Location location;

    @Override
    public int compareTo(BaseEntity o) {
        return id.compareTo(o.id);
    }
}
