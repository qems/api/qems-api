package de.offis.e.esc.qems.api.entities.controls;

import de.offis.mosaik.api.utils.generics.Model;
import lombok.Builder;
import lombok.Data;

import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public sealed interface Read extends Capability {
    @Model @Data final class Now implements Read {Double value; String Id = UUID.randomUUID().toString();}
    @Model @Data final class Historic implements Read {Map<Long, Double> history; String Id = UUID.randomUUID().toString();}
    @Model @Data final class Prediction implements Read {Map<Long, Double> forecast; String Id = UUID.randomUUID().toString();}
}


