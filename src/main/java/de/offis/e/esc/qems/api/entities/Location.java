package de.offis.e.esc.qems.api.entities;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Location {
    public Double latitude;
    public Double longitude;
    public Double altitude;
}
