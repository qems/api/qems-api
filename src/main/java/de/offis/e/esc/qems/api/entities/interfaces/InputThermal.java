package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface InputThermal {
    Double getInputThermalMin();
    void setInputThermalMin(Double val);
    Double getInputThermalMax();
    void setInputThermalMax(Double val);
    SortedSet<Capability> getInputThermalPowerW();
    void setInputThermalPowerW(SortedSet<Capability> value);
}
