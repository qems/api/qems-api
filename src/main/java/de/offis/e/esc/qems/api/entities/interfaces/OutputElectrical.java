package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface OutputElectrical {

    Double getOutputElectricalMin();
    void setOutputElectricalMin(Double val);
    Double getOutputElectricalMax();
    void setOutputElectricalMax(Double val);
    SortedSet<Capability> getOutputElectricalPowerW();
    void setOutputElectricalPowerW(SortedSet<Capability> value);
}
