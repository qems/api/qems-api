package de.offis.e.esc.qems.api.entities.battery;

import de.offis.e.esc.qems.api.entities.BaseEntity;
import de.offis.e.esc.qems.api.entities.Location;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.interfaces.InputElectrical;
import de.offis.e.esc.qems.api.entities.interfaces.StorageElectrical;
import de.offis.mosaik.api.utils.generics.Model;
import de.offis.e.esc.qems.api.entities.interfaces.OutputElectrical;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

@Model
@Data
@EqualsAndHashCode(callSuper = true)
public class Battery extends BaseEntity implements InputElectrical, OutputElectrical, StorageElectrical {
    Double inputElectricalMin;
    Double inputElectricalMax;
    SortedSet<Capability> inputElectricalPowerW;

    Double outputElectricalMin;
    Double outputElectricalMax;
    SortedSet<Capability> outputElectricalPowerW;

    Double storageElectricalCapacityWh;
    SortedSet<Capability> storageElectricalSoC;
    SortedSet<Capability> storageElectricalSoCWh;
    Double storageElectricalSelfDischarge;

    @Builder
    @Model.Constructor
    public Battery(@Model.Id @Model.Param("id") String id,
                   @Model.Param("maxElectricalPowerOutputW") Double maxElectricalPowerOutputW,
                   @Model.Param("capacityWh") Double capacityWh,
                   @Model.Param("selfDischarge") Double selfDischarge,
                   @Model.Param("latitude") Double latitude,
                   @Model.Param("longitude") Double longitude,
                   @Model.Param("altitude") Double altitude,
                   @Model.Param("outputElectricalPowerW") Map<String, Object> outputElectricalPowerW,
                   @Model.Param("inputElectricalPowerW") Map<String, Object> inputElectricalPowerW,
                   @Model.Param("storageElectricalSoC") Map<String, Object> storageElectricalSoC,
                   @Model.Param("storageElectricalSoCWh") Map<String, Object> storageElectricalSoCWh
    ) {
        setId(id);
        setInputElectricalMin(0d);
        setInputElectricalMax(maxElectricalPowerOutputW);
        Capability.setCapabilities(inputElectricalPowerW, this::setInputElectricalPowerW);

        Capability.setCapabilities(storageElectricalSoCWh, this::setStorageElectricalSoCWh);
        Capability.setCapabilities(storageElectricalSoC, this::setStorageElectricalSoC);
        setStorageElectricalCapacityWh(capacityWh);
        setStorageElectricalSelfDischarge(selfDischarge);

        setOutputElectricalMin(0d);
        setOutputElectricalMax(maxElectricalPowerOutputW);
        Capability.setCapabilities(outputElectricalPowerW, this::setOutputElectricalPowerW);

        var location = Location.builder()
                .altitude(altitude)
                .latitude(latitude)
                .longitude(longitude)
                .build();
        setLocation(location);
    }


}
