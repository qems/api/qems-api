package de.offis.e.esc.qems.api.entities.interfaces;

import de.offis.e.esc.qems.api.entities.controls.Capability;

import java.util.Set;
import java.util.SortedSet;

public interface Transformer {
    Double getCopDefinition();
    void setCopDefinition(Double value);
    void setCop(SortedSet<Capability> value);
    SortedSet<Capability> getCop();
}
