package de.offis.e.esc.qems.api.entities.controls;

import java.util.*;
import java.util.function.Consumer;

public sealed interface Capability extends Comparable<Capability> permits ControlSchedule, Control, Read {
    String getId();
    @Override
    default int compareTo(Capability o) {
        int classCompare = this.getClass().getName().compareTo(o.getClass().getName());
        if (classCompare == 0) {
            return this.getId().compareTo(o.getId());
        }
        return classCompare;
    }
    default String getCapability() {
        return this.getClass().getSimpleName();
    }
    static <T extends Capability> Optional<T> getCapability(SortedSet<Capability> capabilities, Class<T> chosenControl) {
        return capabilities.stream().filter(chosenControl::isInstance)
                .map(chosenControl::cast)
                .findFirst();
    }
    static Optional<Double> getCurrentReadValue(SortedSet<Capability> capabilities) {
        return getCapability(capabilities, Read.Now.class).map(Read.Now::getValue);
    }
    static void setCapabilities(Map<String, Object> capabilitiesMap, Consumer<SortedSet<Capability>> setFunction) {
        SortedSet<Capability> capabilities = new TreeSet<>();
        List<Map.Entry<String, Object>> entryList = new ArrayList<>();
        for (var entry :
                capabilitiesMap.entrySet()) {
            if (!Objects.equals(entry.getKey(), "Schedule")) {
                capabilities.add(ParseUtils.parseControls(entry.getKey(), entry.getValue(), capabilities));
            } else {
                entryList.add(entry);
            }
        }
        // Schedules must be initialized at a later stage due to the dependency to other capabilities as control
        for (var entry : entryList) {
            capabilities.add(ParseUtils.parseControls(entry.getKey(), entry.getValue(), capabilities));
        }
        setFunction.accept(capabilities);
    }
}

